//
//  AboutView.swift
//  Bullseye
//
//  Created by Nick Beznos on 2/21/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import SwiftUI

struct AboutView: View {
    
    let backgroundColor = Color(red: 255.0/255.0, green: 214.0/255.0, blue: 179.0/255.0)
    
    struct HeadingTextStyle: ViewModifier {
        func body(content: Content) -> some View {
            return content
                .foregroundColor(.black)
                .font(Font.custom("Arial Rounded MT Bold", size: 30))
        }
    }
    
    struct TextViewsStyle: ViewModifier {
        func body(content: Content) -> some View {
            return content
                .foregroundColor(.black)
                .font(Font.custom("Arial Rounded MT Bold", size: 16))
        }
    }
    
    var body: some View {
        VStack {
            Text("🎯Bulls eye🎯").modifier(HeadingTextStyle())
            padding(.top, 20)
            padding(.bottom, 20)
            
            VStack{
                Text("🎯Bulls eye🎯").modifier(TextViewsStyle())
                Text("🎯Bulls eye🎯").modifier(TextViewsStyle())
                Text("🎯Bulls eye🎯").modifier(TextViewsStyle())
            }
            padding(.leading, 60)
            padding(.trailing, 60)
            padding(.bottom, 20)

        }

        .navigationBarTitle("About Bullseye")
        .background(backgroundColor)
    }
}

struct AboutView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().previewLayout(.fixed(width: 896, height: 414))
    }
}
