//
//  ContentView.swift
//  Bullseye
//
//  Created by Nick Beznos on 2/19/20.
//  Copyright © 2020 Nick Beznos. All rights reserved.
//

import SwiftUI


struct ContentView: View {
    @State var alertIsVisible = false
    @State var sliderValue = 50.0
    @State var target = Int.random(in: 1...100)
    @State var totalScore = 0
    @State var currentRound = 1
    
    let midnightBlue = Color(red: 0.0/255.0, green: 51.0/255.0, blue: 102.0/255.0)
    
    
    struct LabelStyle: ViewModifier {
        func body(content: Content) -> some View {
            return content
                .foregroundColor(.white)
                .modifier(Shadow())
                .font(Font.custom("Arial Rounded MT Bold", size: 18))
        }
    }
    
    struct ValueStyle: ViewModifier {
        func body(content: Content) -> some View {
            return content
                .foregroundColor(.yellow)
                .modifier(Shadow())
                .font(Font.custom("Arial Rounded MT Bold", size: 24))
        }
    }
    
    
    struct Shadow: ViewModifier {
        func body(content: Content) -> some View {
            return content
                .shadow(color: .black, radius: 5, x: 2, y: 2)
        }
    }

    struct LargeButtonTextStyle: ViewModifier {
        func body(content: Content) -> some View {
            return content
                .foregroundColor(.black)
                .font(Font.custom("Arial Rounded MT Bold", size: 18))
        }
    }
    
    struct SmallButtonTextStyle: ViewModifier {
        func body(content: Content) -> some View {
            return content
                .foregroundColor(.black)
                .font(Font.custom("Arial Rounded MT Bold", size: 12))
        }
    }

    var body: some View {
        VStack {
                Spacer()

                HStack {
                    Text("Put the bullseye as close as you can to:").modifier(LabelStyle())
                    Text("\(target)").modifier(ValueStyle())
                }
                Spacer()

                
                HStack {
                    Text("1").modifier(LabelStyle())
                    Slider(value: $sliderValue, in: 0...100)
                    Text("100").modifier(LabelStyle())
                }
                Spacer()

                
                Button(action: {
                    self.alertIsVisible = true
                }) {
                    Text("Hit me").modifier(LargeButtonTextStyle())
                        .background(Image("Button")).modifier(Shadow())
                }
                .buttonStyle(PlainButtonStyle())
                    
                .alert(isPresented: $alertIsVisible) { () -> Alert in
                    return Alert(title: Text(alertTitle()), message: Text("Slider value is \(sliderValueRounded())." +
                        "You scored \(pointsForCurrentRound()) points this round."), dismissButton: .default(Text("Ok")) {
                            self.totalScore = self.totalScore + self.pointsForCurrentRound()
                            self.target = Int.random(in: 1...100)
                        })
                }
                Spacer()

                HStack {
                    Button(action: {
                        self.newGame()
                    }) {
                        HStack {
                            Image("StartOverIcon")
                            Text("Start over").modifier(SmallButtonTextStyle())
                        }
                        .background(Image("Button")).modifier(Shadow())
                    }
                    .buttonStyle(PlainButtonStyle())
                    
                    Spacer()
                    Text("Score:").modifier(LabelStyle())
                    Text("\(totalScore)").modifier(ValueStyle())
                    Spacer()
                    Text("Round:").modifier(LabelStyle())
                    Text("\(currentRound)").modifier(ValueStyle())
                    Spacer()
                    
                    // MARK: NavigationLink wasn't working because of apples bugs
                    NavigationLink(destination: AboutView()) {
                        HStack {
                            Image("InfoIcon")
                            Text("Info").modifier(SmallButtonTextStyle())
                        }
                        .background(Image("Button")).modifier(Shadow())
                    }
                    .buttonStyle(PlainButtonStyle())
                    
                }
                
                .padding(.bottom, 20)
                Spacer()

        }
        .accentColor(midnightBlue)
        .background(Image("Background"), alignment: .center)
        .navigationBarTitle("Bullseye")
    }
    
    
    func amountOf() -> Int {
        return abs(target - sliderValueRounded())
    }
    
    
    func sliderValueRounded() -> Int {
        return Int(sliderValue.rounded())
    }
    
    
    func pointsForCurrentRound() -> Int {
        let bonus: Int
        let maxScore = 100
        
        if amountOf() == 0 {
            bonus = 100
        } else if amountOf() == 1 {
            bonus = 50
        } else {
            bonus = 0
        }
        
        return maxScore - amountOf() + bonus
    }

    
    func alertTitle() -> String {
        let difference = amountOf()
        let title: String
        
        if difference == 0 {
            title = "Heck, you're good"
        } else if difference < 5 {
            title = "You almost had it"
        } else if difference <= 10 {
            title = "Not bad"
        } else {
            title = "U suck nuddles at bullseye, go check your eye doctor!"
        }
        
        return title
    }
    
    
    func newGame() {
        totalScore = 0
        currentRound += 1
        target = Int.random(in: 1...100)
        sliderValue = 50.0
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().previewLayout(.fixed(width: 896, height: 414))
    }
}


